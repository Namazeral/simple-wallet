import environs

# reading .env file
env = environs.Env()
environs.Env.read_env()


class Config(object):
    """
    how to set config: https://hackersandslackers.com/configure-flask-applications/
    """
    SECRET_KEY = env('SECRET_KEY')
    SQLALCHEMY_DATABASE_URI = env('SQLALCHEMY_DATABASE_URI')
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    SQLALCHEMY_RECORD_QUERIES = True

    # JWT
    JWT_EXPIRED = 60 * 24 * 365


class ProductionConfig(Config):
    FLASK_ENV = 'production'
    DEVELOPMENT = False
    DEBUG = False


class StagingConfig(Config):
    FLASK_ENV = 'staging'
    DEVELOPMENT = True
    DEBUG = True


class DevConfig(Config):
    FLASK_ENV = 'development'
    DEVELOPMENT = True
    DEBUG = True