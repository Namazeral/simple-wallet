import datetime
import random
import time
import pytz


def generate_api_call_id():
    """
    Generate unique api call id
    """
    now_time = int(time.time())
    current_date = datetime.datetime.now()

    hour_tm = current_date.hour
    minute_tm = current_date.minute
    second_tm = current_date.second

    start_id = str(now_time) + str(hour_tm) + str(minute_tm) + str(second_tm)
    random_num = random.randint(1, 10000000)
    invoice_code = 'API_CALL_{}_{}'.format(str(start_id), str(random_num))
    return invoice_code


def get_timezone(timezone: str) -> pytz.timezone:
    try:
        tz = pytz.timezone(timezone)
        return tz
    except Exception:
        return pytz.timezone('Asia/Jakarta')
