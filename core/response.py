import copy
import datetime
from flask import Flask
from typing import Dict

from core.utils import (
    generate_api_call_id,
    get_timezone,
)

app = Flask(__name__)
RESPONSE_SUCCESS = {
    'SUCCESS': {
        'status': 200,
        'code': 'SUCCESS',
    }
}

RESPONSE_ERROR = {
    'GENERAL_ERROR_REQUEST': {
        'status': 510,
        'code': 'GENERAL_ERROR_REQUEST',
    },
    'INVALID_REQUEST_PARAMETER': {
        'status': 400,
        'code': 'INVALID_REQUEST_PARAMETER',
    },
    'AUTHENTICATIONS_FAILED': {
        'status': 401,
        'code': 'AUTHENTICATION_FAIL',
    },
    'WALLET_DISABLED': {
        'status': 401,
        'code': 'WALLET_DISABLED',
    },
    'WALLET_ALREADY_ENABLE': {
        'status': 401,
        'code': 'WALLET_ALREADY_ENABLE',
    },
    'BALANCE_INSUFFICIENT': {
        'status': 401,
        'code': 'BALANCE_INSUFFICIENT',
    },
    'DUPLICATE_REFFERENCE_ID': {
        'status': 401,
        'code': 'DUPLICATE_REFFERENCE_ID',
    },
}


class MyRequest:

    def __init__(self):
        self.date_time_req = datetime.datetime.now()
        self.full_path = None
        self.payload = None
        self.param = None
        self.header = None
        self.remote_addr = None
        self.response = {}

    def get_request(self, my_request):
        self.full_path = my_request.full_path
        self.remote_addr = my_request.remote_addr
        self.param = my_request.args.to_dict()
        if my_request.json:
            self.payload = my_request.json
        elif my_request.form:
            self.payload = my_request.form
        else:
            self.payload = {}
        self.header = my_request.headers
        app.logger.debug('------------- REQUEST -------------')
        app.logger.debug('fullpath: {}'.format(self.full_path))
        app.logger.debug('param: {}'.format(self.param))
        app.logger.debug('header: {}'.format(dict(self.header)))
        app.logger.debug('body: {}'.format(self.payload))
        app.logger.debug('------------- REQUEST -------------')
        return

    def put(self, key, value):
        if not (key in self.response):
            raise ValueError('SETTING_NON_EXISTING_FIELD', key, value)

        self.response[key] = value

    def set_resp(self, action, data):
        self.response.update({
            'status': action,
            'data': data,
        })

    def stringify_v1(self):
        date_now = datetime.datetime.now()
        delta_time = abs((date_now - self.date_time_req).microseconds)
        app.logger.debug("------------------Response------------------")
        app.logger.debug(self.response)
        app.logger.debug(f'{self.full_path} time taken: {delta_time} ms')
        app.logger.debug("------------------Response------------------")
        return self.response

    def construct_error_message(self, code: str, default_code=None):
        error_resp = dict()
        app.logger.debug("------------------CONSTRUCT ERROR------------------")
        app.logger.debug(code)
        errors = RESPONSE_ERROR.get(code)

        if not errors and default_code:
            errors = RESPONSE_ERROR.get(default_code)

        if errors:
            error_resp['code'] = errors.get('code')

        self.put('status', error_resp.get('code'))

        return error_resp.get('status')