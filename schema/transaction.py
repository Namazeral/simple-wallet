from marshmallow import (
    fields,
    Schema,
)


class TransactionSchema(Schema):
    amount = fields.String(required=True)
    reference_id = fields.String(required=True)


class GetDepositDetailSchema(Schema):
    deposited_by = fields.Method("get_deposited_by")
    deposited_at = fields.Method("get_deposited_at")
    status = fields.Method("get_account_status_display")

    @staticmethod
    def get_account_status_display(obj):
        return obj.transaction_status.value

    @staticmethod
    def get_deposited_by(obj):
        return obj.account_id

    @staticmethod
    def get_deposited_at(obj):
        return obj.created_at

    class Meta:
        # Fields to expose
        fields = ('id', 'deposited_by', 'status', 'deposited_at', 'amount', 'reference_id')


class GetWithdrawalDetailSchema(Schema):
    withdrawn_by = fields.Method("get_withdrawn_by")
    withdrawn_at = fields.Method("get_withdrawn_at")
    status = fields.Method("get_account_status_display")

    @staticmethod
    def get_account_status_display(obj):
        return obj.transaction_status.value

    @staticmethod
    def get_withdrawn_by(obj):
        return obj.account_id

    @staticmethod
    def get_withdrawn_at(obj):
        return obj.created_at

    class Meta:
        # Fields to expose
        fields = ('id', 'withdrawn_by', 'status', 'withdrawn_at', 'amount', 'reference_id')