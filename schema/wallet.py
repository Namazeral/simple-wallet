from marshmallow import (
    fields,
    Schema,
)


class GetWalletDetailSchema(Schema):
    id = fields.Method("get_wallet_id")
    owned_by = fields.Method("get_owned_by")
    status = fields.Method("get_user_status_display")
    balance = fields.Method("get_balance")

    @staticmethod
    def get_user_status_display(obj):
        return obj.wallet_status.value

    @staticmethod
    def get_balance(obj):
        if not obj.balance:
            return 0
        else:
            return obj.balance

    @staticmethod
    def get_owned_by(obj):
        return obj.account_id

    @staticmethod
    def get_wallet_id(obj):
        return obj.wallet_id

    class Meta:
        # Fields to expose
        fields = ('id', 'owned_by', 'status', 'enabled_at', 'balance')