from marshmallow import (
    fields,
    Schema,
)


class AuthSchema(Schema):
    customer_xid = fields.String(required=True)