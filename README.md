# SIMPLE WALLET

## Prerequisites

[![Code](https://img.shields.io/badge/Code-Python-1B9D73?style=flat&logo=python)](https://python.org)
[![Framework](https://img.shields.io/badge/Framework-Flask-1B9D73?style=flat&logo=flask)](https://flask.palletsprojects.com/)


## How to run
Make sure you already installed **python3.8** in your machine

1. Create your virtualenv and activate
2. Install library `pip install -r requirements.txt`
3. create .env file <br/> 
   `SECRET_KEY = 't1NP63m4wnBg6nyHYKfmc2TpCOGI4nss'`<br/> 
   `SQLALCHEMY_DATABASE_URI = "postgresql://postgres:password@localhost:5432/simple_wallet"`<br/> 
   `APP_SETTINGS = core.config.DevConfig # change to core.config.ProductionConfig for production`
4. Create new database with postgresql `create database simple_wallet;`
5. init flask migrate file location `flask db init -d "models/migrations"`
    <br> if not yet and only for the first time, check `./models/migrations`
6. create your start-app.sh file and run server command `sh ./start-app.sh` 


### How to run server manually
1. set your Env Variabel key for `Flask_APP` and `FLASK_ENV` 
    <br> `$env:FLASK_APP="server.py"`
    <br> `$env:FLASK_ENV="development"`
2. migrate your db
    <br> `flask db upgrade -d "models/migrations"`
3. run server
    <br> `flask run --host=localhost --port=3000`
   
### How to Manage ORM DB 
1. make migrate file (alembic), for any change in your models
    <br>`flask db migrate -d "models/migrations" -m "message"`
    <br> recheck your migrate file in `./models/migrations/versions`, cz cannot detect rename table/column automatically.
2. execute migration for your change in db
    <br>`flask db upgrade -d "models/migrations"`

after make ORM class for table [`path: ./models]
<br> make sure your ORM class is registered in table register at app.py -> register_table
<br> if your new ORM class isnot reistered, you cann't migrate your db
<br><br>
