from flask import request, Flask, Blueprint
from core.response import MyRequest
from services.wallet import WalletService
from services.transaction import TransactionService


app = Flask(__name__)
wallet_bp = Blueprint('wallet', __name__, url_prefix='/wallet')


@wallet_bp.route('/ping')
def ping():
    return {'message': 'Hello World!'}


@wallet_bp.route('', methods=['GET', 'POST', 'PATCH'])
def wallet():
    myreq = MyRequest()
    myreq.get_request(request)

    if request.method == 'POST':
        wallet = WalletService(myreq=myreq)
        resp, status_code = wallet.enable_wallet()
        return resp.stringify_v1(), status_code

    elif request.method == 'GET':
        wallet = WalletService(myreq=myreq)
        resp, status_code = wallet.get_wallet()
        return resp.stringify_v1(), status_code

    elif request.method == 'PATCH':
        wallet = WalletService(myreq=myreq)
        resp, status_code = wallet.disable_wallet()
        return resp.stringify_v1(), status_code


@wallet_bp.route('/deposits', methods=['POST'])
def deposit():
    myreq = MyRequest()
    myreq.get_request(request)

    if request.method == 'POST':
        transaction = TransactionService(myreq=myreq)
        resp, status_code = transaction.deposit()
        return resp.stringify_v1(), status_code

@wallet_bp.route('/withdrawals', methods=['POST'])
def withdrawal():
    myreq = MyRequest()
    myreq.get_request(request)

    if request.method == 'POST':
        transaction = TransactionService(myreq=myreq)
        resp, status_code = transaction.withdrawal()
        return resp.stringify_v1(), status_code
