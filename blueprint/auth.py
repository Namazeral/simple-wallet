from flask import request, Flask, Blueprint
from core.response import MyRequest
from services.auth import AuthService


app = Flask(__name__)
auth_bp = Blueprint('init', __name__, url_prefix='/init')


@auth_bp.route('/ping')
def ping():
    return {'message': 'Hello World!'}


@auth_bp.route('', methods=['POST'])
def create_account():
    myreq = MyRequest()
    myreq.get_request(request)

    if request.method == 'POST':
        auth = AuthService(myreq=myreq)
        resp, status_code = auth.create_account()
        return resp.stringify_v1(), status_code