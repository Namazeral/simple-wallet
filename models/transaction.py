from .db import Column, PkModel, db, BaseEnum, ForeignKey, relationship


class TransactionType(BaseEnum):
    deposit = "deposit"
    withdrawals = "withdrawals"


class TransactionStatus(BaseEnum):
    success = "success"
    pending = "pending"
    failed = 'failed'


class Transaction(PkModel):
    __tablename__ = 'transaction'

    id = Column(db.Integer, primary_key=True, index=True)
    account_id = Column(db.String(), ForeignKey('account.account_id'))
    transaction_type = Column('transaction_type', db.Enum(TransactionType))
    transaction_status = Column('transaction_status', db.Enum(TransactionStatus))
    amount = Column(db.Float())
    reference_id = Column(db.String(), nullable=False, unique=True)

    account = relationship("Account", backref="account")
    created_at = db.Column(db.DateTime, server_default=db.func.now())
    updated_at = db.Column(db.DateTime, server_default=db.func.now(), server_onupdate=db.func.now())

    def __repr__(self):
        """Represent instance as a unique string."""
        return f"<{self.reference_id}>"

    def save(self, commit=True):
        """Save the record."""
        db.session.add(self)
        if commit:
            db.session.commit()
        return self