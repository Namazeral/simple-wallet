from .db import Column, PkModel, db, BaseEnum


class WalletStatus(BaseEnum):
    enabled = "enabled"
    disabled = "disabled"


class Account(PkModel):
    __tablename__ = 'account'

    id = Column(db.Integer, primary_key=True, index=True)
    account_id = Column(db.String(), nullable=False, unique=True)
    wallet_id = Column(db.String(), nullable=False, unique=True)
    wallet_status = Column('wallet_status', db.Enum(WalletStatus))
    enabled_at = db.Column(db.DateTime)
    disabled_at = db.Column(db.DateTime)
    balance = Column(db.Float())

    created_at = db.Column(db.DateTime, server_default=db.func.now())
    updated_at = db.Column(db.DateTime, server_default=db.func.now(), server_onupdate=db.func.now())

    def __repr__(self):
        """Represent instance as a unique string."""
        return f"<{self.account_id}>"

    def save(self, commit=True):
        """Save the record."""
        db.session.add(self)
        if commit:
            db.session.commit()
        return self


class AccountQuery(object):

    @staticmethod
    def get_one_account_by_account_id(account_id):
        account = Account.query.filter(Account.account_id == account_id).first()
        return account
