import traceback
import datetime
import jwt
import uuid
from flask import request
from flask import current_app as app

from core.response import MyRequest
from schema.auth import AuthSchema
from models.account import WalletStatus, Account, AccountQuery


class AuthService:

    def __init__(self, myreq=None):
        self.myreq = myreq
        if not myreq:
            self.myreq = MyRequest()
            self.myreq.get_request(request)

        myreq.set_resp('GENERAL_ERROR_REQUEST', {})
        self.status_code = 510
        self.header = myreq.header
        self.param = myreq.param
        self.payload = myreq.payload
        self.customer_xid = None

    def _generate_login_token(self):
        access_exp = app.config['JWT_EXPIRED']
        access_payload = {
            # 'exp': datetime.datetime.utcnow() + datetime.timedelta(minutes=access_exp),
            'iat': datetime.datetime.utcnow(),
            'sub': self.customer_xid,
        }
        access_token = jwt.encode(access_payload, app.config['SECRET_KEY'], algorithm='HS256')
        return access_token

    def _save_account(self):
        account_payload = dict(
            account_id=self.customer_xid,
            wallet_id=str(uuid.uuid4().hex),
            wallet_status=WalletStatus.disabled.value,
            balance=0.0
        )
        new_account = Account(**account_payload).save()
        return new_account

    def verify_token(self, auth_token=None, validate_wallet=False):
        if not auth_token:
            auth_token = self.header.get('Authorization').split(' ')[1]
        payload_jwt = jwt.decode(auth_token, app.config['SECRET_KEY'], algorithms='HS256',
                                 options={'verify_exp': False})

        token_account_id = str(payload_jwt.get('sub'))
        account_active = AccountQuery.get_one_account_by_account_id(token_account_id)
        if validate_wallet and account_active.wallet_status.value == WalletStatus.disabled.value:
            raise ValueError('WALLET_DISABLED')

        if not account_active:
            raise ValueError('CREDENTIAL_ID_NOT_FOUND')

        return account_active

    def create_account(self):
        try:
            raw_body = AuthSchema().load(self.payload)
            body = AuthSchema().dump(raw_body)
            self.customer_xid = body['customer_xid']
            account = AccountQuery.get_one_account_by_account_id(self.customer_xid)
            if not account:
                self._save_account()
            access_token = self._generate_login_token()

            resp_data = {"token": access_token}
            self.myreq.set_resp('SUCCESS', resp_data)
            self.status_code = 200

        except Exception as err:
            class_exc = type(err).__name__
            app.logger.error(err)
            app.logger.error(traceback.format_exc())
            if class_exc == 'ValidationError':  # for validation error on schema
                self.status_code = self.myreq.construct_error_message('INVALID_REQUEST_PARAMETER')
            else:
                self.status_code = self.myreq.construct_error_message(str(err), 'GENERAL_ERROR_REQUEST')

        return self.myreq, self.status_code
