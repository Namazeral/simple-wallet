import traceback
import datetime
import jwt
import uuid
from flask import request
from flask import current_app as app

from core.response import MyRequest
from services.auth import AuthService
from models.account import WalletStatus
from schema.wallet import GetWalletDetailSchema


class WalletService:

    def __init__(self, myreq=None):
        self.myreq = myreq
        if not myreq:
            self.myreq = MyRequest()
            self.myreq.get_request(request)

        myreq.set_resp('GENERAL_ERROR_REQUEST', {})
        self.status_code = 510
        self.header = myreq.header
        self.param = myreq.param
        self.payload = myreq.payload
        self.customer_xid = None

    def enable_wallet(self):
        try:
            auth = AuthService(myreq=self.myreq)
            account = auth.verify_token()

            if account.wallet_status.value == WalletStatus.enabled.value:
                raise ValueError('WALLET_ALREADY_ENABLE')

            account.wallet_status = WalletStatus.enabled.value
            account.enabled_at = datetime.datetime.now()
            account.save()

            wallet_schema = GetWalletDetailSchema()
            wallet_detail = wallet_schema.dump(account)

            resp_data = {"wallet": wallet_detail}
            self.myreq.set_resp('SUCCESS', resp_data)
            self.status_code = 200

        except Exception as err:
            class_exc = type(err).__name__
            app.logger.error(err)
            app.logger.error(traceback.format_exc())
            if class_exc == 'ValidationError':  # for validation error on schema
                self.status_code = self.myreq.construct_error_message('INVALID_REQUEST_PARAMETER')
            else:
                self.status_code = self.myreq.construct_error_message(str(err), 'GENERAL_ERROR_REQUEST')

        return self.myreq, self.status_code

    def get_wallet(self):
        try:
            auth = AuthService(myreq=self.myreq)
            account_service = auth.verify_token(validate_wallet=True)

            wallet_schema = GetWalletDetailSchema()
            wallet_detail = wallet_schema.dump(account_service)

            resp_data = {"wallet": wallet_detail}
            self.myreq.set_resp('SUCCESS', resp_data)
            self.status_code = 200

        except Exception as err:
            class_exc = type(err).__name__
            app.logger.error(err)
            app.logger.error(traceback.format_exc())
            if class_exc == 'ValidationError':  # for validation error on schema
                self.status_code = self.myreq.construct_error_message('INVALID_REQUEST_PARAMETER')
            else:
                self.status_code = self.myreq.construct_error_message(str(err), 'GENERAL_ERROR_REQUEST')

        return self.myreq, self.status_code

    def disable_wallet(self):
        try:
            auth = AuthService(myreq=self.myreq)
            account_service = auth.verify_token()

            account_service.wallet_status = WalletStatus.disabled.value
            account_service.enabled_at = datetime.datetime.now()
            account_service.save()

            wallet_schema = GetWalletDetailSchema()
            wallet_detail = wallet_schema.dump(account_service)

            resp_data = {"wallet": wallet_detail}
            self.myreq.set_resp('SUCCESS', resp_data)
            self.status_code = 200

        except Exception as err:
            class_exc = type(err).__name__
            app.logger.error(err)
            app.logger.error(traceback.format_exc())
            if class_exc == 'ValidationError':  # for validation error on schema
                self.status_code = self.myreq.construct_error_message('INVALID_REQUEST_PARAMETER')
            else:
                self.status_code = self.myreq.construct_error_message(str(err), 'GENERAL_ERROR_REQUEST')

        return self.myreq, self.status_code