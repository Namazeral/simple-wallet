import traceback
import datetime
import jwt
import uuid
from flask import request
from flask import current_app as app

from core.response import MyRequest
from services.auth import AuthService
from models.transaction import TransactionStatus, TransactionType, Transaction
from schema.transaction import TransactionSchema, GetDepositDetailSchema, GetWithdrawalDetailSchema


class TransactionService():

    def __init__(self, myreq=None):
        self.myreq = myreq
        if not myreq:
            self.myreq = MyRequest()
            self.myreq.get_request(request)

        myreq.set_resp('GENERAL_ERROR_REQUEST', {})
        self.status_code = 510
        self.header = myreq.header
        self.param = myreq.param
        self.payload = myreq.payload
        self.customer_xid = None
        self.account = None
        self.transaction_type = None
        self.amount = None
        self.reference_id = None

    def _save_transaction(self):
        try:
            transaction_payload = dict(
                account=self.account,
                transaction_type=self.transaction_type,
                transaction_status=TransactionStatus.pending.value,
                amount=self.amount,
                reference_id=self.reference_id,
            )
            pending_transaction = Transaction(**transaction_payload).save()
            return pending_transaction
        except Exception as e:
            raise ValueError(str(e))

    def deposit(self):
        transaction = None
        try:
            auth = AuthService(myreq=self.myreq)
            self.account = auth.verify_token(validate_wallet=True)

            raw_body = TransactionSchema().load(self.payload)
            body = TransactionSchema().dump(raw_body)

            self.amount = float(body['amount'])
            self.reference_id = body['reference_id']
            self.transaction_type = TransactionType.deposit.value

            transaction = self._save_transaction()

            account_balance = float(self.account.balance) + float(self.amount) if self.account.balance else float(self.amount)
            self.account.balance = account_balance
            self.account.save()

            transaction.transaction_status = TransactionStatus.success.value
            transaction.save()

            transaction_schema = GetDepositDetailSchema()
            transaction_detail = transaction_schema.dump(transaction)

            resp_data = {"deposit": transaction_detail}
            self.myreq.set_resp('SUCCESS', resp_data)
            self.status_code = 200

        except Exception as err:
            class_exc = type(err).__name__
            app.logger.error(err)
            app.logger.error(traceback.format_exc())
            if class_exc == 'ValidationError':  # for validation error on schema
                self.status_code = self.myreq.construct_error_message('INVALID_REQUEST_PARAMETER')
            else:
                self.status_code = self.myreq.construct_error_message(str(err), 'GENERAL_ERROR_REQUEST')

            if transaction:
                transaction.transaction_status = TransactionStatus.failed.value
                transaction.save()

        return self.myreq, self.status_code

    def withdrawal(self):
        transaction = None
        try:
            auth = AuthService(myreq=self.myreq)
            self.account = auth.verify_token(validate_wallet=True)

            raw_body = TransactionSchema().load(self.payload)
            body = TransactionSchema().dump(raw_body)

            self.amount = float(body['amount'])
            self.reference_id = body['reference_id']
            self.transaction_type = TransactionType.withdrawals.value

            transaction = self._save_transaction()

            if self.account.balance > self.amount:
                account_balance = float(self.account.balance) - float(self.amount) if self.account.balance else float(self.amount)
                self.account.balance = account_balance
                self.account.save()
            else:
                transaction.transaction_status = TransactionStatus.failed.value
                transaction.save()
                raise ValueError('BALANCE_INSUFFICIENT')

            transaction.transaction_status = TransactionStatus.success.value
            transaction.save()

            transaction_schema = GetWithdrawalDetailSchema()
            transaction_detail = transaction_schema.dump(transaction)

            resp_data = {"deposit": transaction_detail}
            self.myreq.set_resp('SUCCESS', resp_data)
            self.status_code = 200

        except Exception as err:
            class_exc = type(err).__name__
            app.logger.error(err)
            app.logger.error(traceback.format_exc())
            if class_exc == 'ValidationError':  # for validation error on schema
                self.status_code = self.myreq.construct_error_message('INVALID_REQUEST_PARAMETER')
            else:
                self.status_code = self.myreq.construct_error_message(str(err), 'GENERAL_ERROR_REQUEST')

            if transaction:
                transaction.transaction_status = TransactionStatus.failed.value
                transaction.save()

        return self.myreq, self.status_code
