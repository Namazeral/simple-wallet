import environs
from flask import Flask

from blueprint.auth import auth_bp
from blueprint.wallet import wallet_bp
from core.config import ProductionConfig
from models.db import initialize_db, db
from models.account import Account
from models.transaction import Transaction

# Insert the model to the table here
register_table = {
    "Account": Account,
    "Transaction": Transaction
}

# reading .env file
env = environs.Env()
environs.Env.read_env()


def create_app():
    """
    Create application factory,
    as explained here: http://flask.pocoo.org/docs/patterns/appfactories/.
    """
    app = Flask(__name__)
    app.config.from_object(env('APP_SETTINGS', ProductionConfig))
    initialize_db(app)
    register_blueprints(app)
    register_shellcontext(app)
    return app


def register_shellcontext(app):
    """Register shell context objects."""
    shell_context = {'db': db}
    shell_context.update(register_table)
    app.shell_context_processor(lambda: shell_context)


def register_blueprints(app):
    """Register Flask blueprints."""
    app.register_blueprint(auth_bp)
    app.register_blueprint(wallet_bp)
    return None
